"use client";

import Image from "next/image";
import "../assets/navbar.css";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from "@material-tailwind/react";
import { FaPlay } from "react-icons/fa6";
import ReactPlayer from 'react-player';
import { Grid } from '@mui/material';
import React from "react";
import { Swiper, SwiperSlide } from 'swiper/react';
import Link from "next/link";
import DummyThumbnail from "../assets/images/thumbnail.jpeg";
// Import Swiper styles
import { IoSadOutline } from "react-icons/io5";
import NetflixLogo from "../assets/images/BrandLogo.jpg";
import 'swiper/css';
import 'swiper/css/free-mode';
import 'swiper/css/pagination';
import { FreeMode, Pagination } from 'swiper/modules';
import Navbar from "../Component/Navbar"
import MovieThumbnails from "../assets/images/thumbnail.jpeg"
import { useGlobalContext } from "@/ProductContext";
import BookCaraousel from "../assets/images/books.jpg"
export default function Slider() {

    const bottomRef = React.useRef(null);
    const [isClient, setIsClient] = React.useState(false);

    const { bookData } = useGlobalContext();
    const [searchValue, setSearchValue] = React.useState("")
    const [FoundResult, setNotFound] = React.useState("")
    console.log("data--------", bookData.slice(0, 99))
    const [filteredData, setFilteredData] = React.useState([])


    let arr = bookData.slice(0, 99);

    const handleChangeSearch = (event) => {
        console.log("val---", event.target.value.length)
        bottomRef.current.scrollIntoView({ behavior: 'smooth' });


        setSearchValue(event.target.value)
        let searchedData = arr.filter((element) => {
            console.log("element search----", element.title.toLowerCase(), "---------", searchValue.toLowerCase())
            return element.title.toLowerCase().includes(searchValue.toLowerCase())
        })
        console.log('searchedData length---', searchedData.length)
        if (event.target.value.length === 0) {
            console.log("length-------", event.target.value.length)
            setFilteredData([])
        }
        if (searchedData.length === 0) {
            setNotFound("Not Found")
        } else {
            setFilteredData(searchedData)
            console.log("filteredData----------", filteredData)
        }

    }
    const BgStyle1 = {
    }
    const BgStyle2 = {
        backgroundImage: 'url("https://images.unsplash.com/photo-1497436072909-60f360e1d4b1?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2560&q=80")',
        height: "500px"
    }

    const BgStyle3 = {
        backgroundImage: 'url("https://images.unsplash.com/photo-1497436072909-60f360e1d4b1?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2560&q=80")',
        height: "500px"
    }

    console.log("books array-----", bookData)

    React.useEffect(() => {
        setIsClient(true);
    }, []);

    if (!isClient) {
        return null; // Return nothing on the server
    }



    return (
        <div>

            <div className="mainNavbar">
                <div className="container mx-auto navbar">
                    <div className="brand">
                        <Image src={NetflixLogo}></Image>
                    </div>
                    <ul className="navlist">
                        <li>Home</li>
                        <li>Series</li>
                        <li>Films</li>
                        <li>category</li>
                    </ul>
                    <div className="searchBox">
                        <input onChange={event => handleChangeSearch(event)} type="search" name="search" id="search" autocomplete="username" className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6 SearchInput" placeholder="Search Books" />
                        <button type="submit" className="searchBtn">Search</button>
                    </div>
                </div>
            </div>


            <Carousel>
                <div style={BgStyle1}>
                    <div className="PlayerBox">

                        <Image
                            className="VideoPlayer"
                            src={BookCaraousel} // added &autoplay=1
                            title="YouTube video player"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                            allowFullScreen
                        ></Image>



                        {/* <ReactPlayer url='https://youtu.be/bV0RAcuG2Ao?si=LzyS71KmRMqCsiiG' className="VideoPlayer" /> */}
                        <div className="CaraouselContent">

                            <div className="BannerContent">
                                <h1>Unlocking Android</h1>
                            </div>
                            <div className="MovieDescription">
                                <div className="badge"><p>2018</p></div>
                                <div className="badge"><p>16+</p></div>
                                <div className="badge"><p>4 Seasons</p></div>
                            </div>
                            <div className="ButtonBox">
                                <a href="/books/booksdetails/1" target="__blank">
                                    <button type="submit" className="BtnPlay"><FaPlay />Start Reading</button>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
                <div style={BgStyle2}>

                </div>
                <div style={BgStyle3}>

                </div>

            </Carousel>

            <div className="SwiperContainer">
                <div className="Heading">
                    <h2>New Releases</h2>
                </div>
            </div>


            <div className="SwiperContainer">
                <Swiper
                    slidesPerView={8}
                    spaceBetween={0}
                    freeMode={true}
                    pagination={{
                        clickable: false,
                    }}
                    // modules={[FreeMode, Pagination]}
                    className="mySwiper"
                >
                    {bookData.slice(0, 99).map((element) => {
                        console.log("element----------", element._id)
                        let url = `https://www.amazon.in/Flex-Action-Revised/dp/${element.isbn}`
                        return (<>
                            <SwiperSlide>
                                <div className="Card">
                                    <div className="ThumbnailMovie">
                                        <Link href={`books/booksdetails/${element._id}`} target="__blank" rel="noopener noreferrer">
                                            <img src={element.thumbnailUrl}></img>
                                        </Link>
                                    </div>
                                    <div className="Description">
                                        <p>Title: {element.title}</p>
                                    </div>
                                    <div className="publish">
                                        <p>{element.status.toLowerCase()}{element._id}</p>
                                    </div>

                                </div>
                            </SwiperSlide>
                        </>)
                    })}
                </Swiper>
            </div>

            <div className="SwiperContainer">
                <div className="Heading">
                    <h2>Search Result is here....</h2>
                </div>
            </div>



            <div ref={bottomRef}>
                <Grid container>
                    

                    {filteredData.length > 0 ? filteredData.map((searchedElement) => {
                        console.log("searched----", searchedElement)
                        return (<>
                            <Grid xs={12} md={2}>
                                <div className="Card">
                                    <div className="ThumbnailMovie">
                                        <Link href={`books/booksdetails/${searchedElement._id}`} target="__blank" rel="noopener noreferrer">
                                            <img src={searchedElement.thumbnailUrl}></img>
                                        </Link>
                                    </div>
                                    <div className="Description">
                                        <p>Title: {searchedElement.title}</p>
                                    </div>
                                    <div className="publish">
                                        <p>{searchedElement.status.toLowerCase()}{searchedElement._id}</p>
                                    </div>
                                </div>
                            </Grid>
                        </>)
                    }) : <div className="SwiperContainer">
                        <div className="Heading" style={{ textAlign: "center" }}>
                            <h2>{FoundResult}</h2>
                        </div>
                    </div>}
                </Grid >

            </div>

        </div>
    );
}

import Image from "next/image";
import React from "react";
import "../assets/navbar.css";
import NetflixLogo from "../assets/images/BrandLogo.jpg";
import { useGlobalContext } from "@/ProductContext";

export function Home() {

    const [searchValue, setSearchValue] = React.useState("")
    const { bookData } = useGlobalContext();
    console.log("data--------", bookData.slice(0,99))
    const [filteredData , setFilteredData] = React.useState([]) 

    let arr = bookData.slice(0,99);
    
    const handleChangeSearch = (event) => {
        console.log("val---", event.target.value)
        setSearchValue(event.target.value)
        let searchedData = arr.filter((element)=>{
            console.log("element search----",element.title.toLowerCase(),"---------", searchValue.toLowerCase())
            return element.title.toLowerCase().includes(searchValue.toLowerCase())
        })
        console.log('searchedData---', searchedData)
        setFilteredData(searchedData)
        console.log("filteredData----------", filteredData) 

    }


    return (
        <div>
            <div className="container mx-auto navbar">
                <div className="brand">
                    <Image src={NetflixLogo}></Image>
                </div>
                <ul className="navlist">
                    <li>Home</li>
                    <li>Series</li>
                    <li>Films</li>
                    <li>category</li>
                </ul>
                <div className="searchBox">
                    <input onChange={event => handleChangeSearch(event)} type="search" name="search" id="search" autocomplete="off" className="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6 SearchInput" placeholder="Search Movies" />
                    <button type="submit" className="searchBtn">Search</button>
                </div>
            </div>
        </div>
    );
}

"use client";
import React from "react";
import { useGlobalContext } from "@/ProductContext";
import "../../../assets/bookdetail.css";
import { MdOutlineAttachMoney } from "react-icons/md";
import Link from "next/link";

export default function BookDetails({ params }) {
    const { bookData } = useGlobalContext();
    const [author,setAuhor] = React.useState([])
    let id = params.booksdetails[1];
    console.log("id----", id)
    console.log("id---------", id)
    console.log("bookData----", bookData)
    let bookDetails = bookData[id-1];
    console.log("book details----", bookDetails)

    if (!bookDetails) {
        return <div>Loading...</div>; // Or any other fallback UI
    }

    return (
        <div>
            <div className="containerBook">
                <div className="bookGrid">
                    <div className="bookTitle">
                        <h1 style={{ color: "white" }}>{bookDetails.title}</h1>
                        <p style={{ color: "white" }}>{bookDetails.longDescription.substring(0, 450)}....</p>
                    </div>
                    <div className="bookImage">
                        <img src={bookDetails.thumbnailUrl} alt={bookDetails.title} />
                    </div>
                </div>
                <div className="buttonBox">
                    <Link href={`https://www.amazon.in/Flex-Action-Revised/dp/${bookDetails.isbn}`} target="__blank">
                        <button type="submit" className="buttonSubmit"><MdOutlineAttachMoney />Buy This Book</button>
                    </Link>
                </div>
                <div>
      
                </div>
            </div>
        </div>
    );
}

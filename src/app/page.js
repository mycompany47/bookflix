"use client";
import Image from "next/image";
import { Home as Navbar } from "../../src/Component/Navbar";
import Slider from "@/Component/Slider";
import axios from "axios";
import React, { useContext , useEffect} from 'react';
import { useGlobalContext } from "@/ProductContext";

export default function Home() {

  const {bookData} = useGlobalContext()

  return (
    <>

      <main>
        <Slider />
      </main>
    </>
  );
}

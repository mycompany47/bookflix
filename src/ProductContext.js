"use client";

import React, { createContext, useState, useEffect, useContext } from 'react';
import axios from "axios";
// creating context
export const ProductsContext = createContext();
// creating Provider for root 
export const ProductsProvider = ({ children }) => {
  const [bookData, setBookData] = useState([]);
  


  const getBookDetail = async () => {
    try {
     const res = await axios.get("/data.json");
     setBookData(res.data)
    } catch (error) {
      console.log(error) 
    }
  }

  useEffect(() => {
    getBookDetail()
  }, [])


  return (
    <ProductsContext.Provider value={{ bookData }}>
      {children}
    </ProductsContext.Provider>
  );
};


export const useGlobalContext = () => useContext(ProductsContext)






